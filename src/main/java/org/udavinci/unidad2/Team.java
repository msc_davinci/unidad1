package org.udavinci.unidad2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Team {
    private ArrayList<Runner> runers = new ArrayList<Runner>();
    private int teamNumber;
    Random rand = new Random();

    private Semaphore start = new Semaphore(1);
    private Semaphore run = new Semaphore(0);
    private Object monitor = new Object();

    public Team(int number, Color color, int teamNumber, Semaphore ready)
    {
        this.teamNumber = teamNumber;
        for(int i = 0; i < number; i++)
        {
            runers.add(new Runner(300 + teamNumber * 30, 160 + i * 30, color, teamNumber, i, start, run, monitor, ready));
        }

        for(Runner runner : runers)
            runner.start();
    }

    public void start_running()
    {
        run.release();
    }

    public void Paint(Graphics g)
    {
        for(Runner runner : runers)
            runner.Paint(g);
    }
}
