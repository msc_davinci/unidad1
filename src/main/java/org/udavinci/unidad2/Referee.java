package org.udavinci.unidad2;

import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class Referee extends Thread{
    private ArrayList<Team> teams = new ArrayList<Team>();
    private Semaphore ready;

    public Referee(ArrayList<Team> teams, Semaphore ready)
    {
        this.ready = ready;
        this.teams = teams;
    }

    @Override
    public void run()
    {
        try {
            // Si todo el equipo esta listo, 1er atleta puede iniciar el semáforo
            ready.acquire(teams.size());
            raisePlayersSemaphores();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void raisePlayersSemaphores() {
        for(Team team : teams)
            team.start_running();
    }
}
