package org.udavinci.unidaduno;

class CurrentThreadDemo {
    public static void main(String args[]) {
        Thread t = Thread.currentThread();
        System.out.println("Hilo actual: " + t);
        try {
            for(int n = 5; n > 0; n--) {
                System.out.println("Hilo: " + n + " y el tiempo de espera es de: " + n + " segundos!");
                Thread.sleep(n*1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Interrupción del hilo principal");}
    }
}
