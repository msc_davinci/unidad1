package org.udavinci.unidaduno;

public class NewThread  implements Runnable {
    Thread t;
    NewThread() {
        t = new Thread(this, "Hilo hijo");// Crea un nuevo hilo
        System.out.println("Hilo hijo: " + t);
        t.start(); // Comienza el hilo
    }
    public void run() {//Punto de entrada del segundo hilo
        try {
            for(int i = 5; i > 0; i--) {
                System.out.println("Hilo hijo: " + i);
                Thread.sleep(500); }
        } catch (InterruptedException e) {
            System.out.println("Interrupción del hilo hijo."); }
        System.out.println("Sale del hilo hijo.");
    }
}
