package org.udavinci.unidad3;

import java.io.*;
import java.net.*;

public class ClientTest {

    public static void pausa() {
        try {
            Thread.sleep(3000);
        } catch (Exception ignored) {
        }
    }


    public static void main(String args[]) {
        String welcome, response;
        Client client;
        BufferedReader reader;
        PrintWriter writer;

        client = new Client("localhost", 8001); //la clase Client Implementa el socket cliente al que se le pasa el argumento 0 que contendrá la dirección o nombre del servidor

        try {
//creamos los canales de entrada/salida para comunicarnos con el servidor
            reader = new BufferedReader(new InputStreamReader(client.in));
            writer = new PrintWriter(new OutputStreamWriter(client.out), true);
//leemos la bienvenida que nos da el servidor
            welcome = reader.readLine();
            pausa();
            System.out.println("Mensaje procedente del servidor: '" + welcome + "'");
//para ver el funcionamiento, enviamos los comandos
            System.out.println("Enviando comando NAME");
            writer.println("NAME");
            response = reader.readLine();
            System.out.println("Respuesta del servidor: '" + response + "'");
//pausa
            pausa();

            System.out.println("Enviando comando HELP");
            writer.println("HELP");
            response = reader.readLine();
            System.out.println("Respuesta del servidor: '" + response + "'");
            pausa();

            System.out.println("Enviando comando DATE");
            writer.println("DATE");
            response = reader.readLine();
            System.out.println("Respuesta del servidor: '" + response + "'");
            pausa();

//el siguiente comando no es entendido por el protocolo implementado por el servidor//como vemos en la respuesta que nos devuelve

            System.out.println("Enviando comando xl");
            writer.println("xl");
            response = reader.readLine();
            System.out.println("Respuesta del servidor: '" + response + "'");
            pausa();

            System.out.println("Enviando comando QUIT");
            writer.println("QUIT");


        } catch (IOException e) {
            System.out.println("IOException en client.in.readln()");
            System.out.println(e);
        }
        try {
            Thread.sleep(2000);
        } catch (Exception ignored) {
        }
    }
}

//-------------------------------------------------------------------
class Client {

    // establece los canales de entrada y de salida a disposicion de las clases de usuario
    public InputStream in;
    public OutputStream out;

    // El socket cliente
    private Socket client;

    public Client(String host, int port) {
        try {
            client = new Socket(host, port);
            System.out.println("Datos del socket: " + client);
            in = client.getInputStream();
            out = client.getOutputStream();
        } catch (IOException e) {
            System.out.println("IOExc : " + e);
        }
    }
}
